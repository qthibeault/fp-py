from __future__ import annotations

import sys
from enum import Enum, auto
from typing import Callable, Sequence, TypeVar

if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

_T = TypeVar("_T", contravariant=True)


class Ordering(Enum):
    LOWER = auto()
    EQUAL = auto()
    HIGHER = auto()


class Ord(Protocol[_T]):
    def compare(__a: _T, __b: _T) -> Ordering:
        ...


def sort(ord: Ord[_T]) -> Callable[[Sequence[_T]], Sequence[_T]]:
    def _sort(xs: Sequence[_T]) -> Sequence[_T]:
        pass

    return _sort
