from __future__ import annotations

import sys
from typing import Callable, Generic, Sequence, TypeVar

if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

_T = TypeVar("_T", contravariant=True)
_U = TypeVar("_U")


class Eq(Protocol[_T]):
    def equals(self, __a: _T, __b: _T) -> bool:
        ...


def eqFrom(func: Callable[[_T, _T], bool]) -> Eq[_T]:
    class NewEq(Eq[_T]):
        def equals(self, a: _T, b: _T) -> bool:
            return func(a, b)

    return NewEq()


def contains(eq: Eq[_T]) -> Callable[[Sequence[_T], _T], bool]:
    def _contains(xs: Sequence[_T], v: _T) -> bool:
        return any(eq.equals(v, x) for x in xs)

    return _contains


def map(eq: Eq[_T]) -> Callable[[Callable[[_U], _T]], Eq[_U]]:
    def _map(func: Callable[[_U], _T]) -> Eq[_U]:
        return eqFrom(lambda a, b: eq.equals(func(a), func(b)))

    return _map
