from __future__ import annotations

import math

from fp_py.eq import Eq, contains


class Rectangle:
    def __init__(self, height: float, width: float):
        self.height = height
        self.width = width

    def area(self) -> float:
        return self.height * self.width


class Square(Rectangle):
    def __init__(self, side: float):
        super().__init__(side, side)


class RectangleEq(Eq[Rectangle]):
    def equals(self, a: Rectangle, b: Rectangle) -> bool:
        return a.height == b.height and a.width == b.width


def test_rect_eq():
    r1 = Rectangle(10, 20)
    r2 = Rectangle(20, 30)
    eq = RectangleEq()

    assert eq.equals(r1, r1)
    assert not eq.equals(r1, r2)


def test_square_eq():
    s1 = Square(5)
    s2 = Square(10)
    eq = RectangleEq()

    assert eq.equals(s1, s1)
    assert not eq.equals(s1, s2)


def test_mixed_eq():
    s1 = Square(5)
    r1 = Rectangle(5, 5)
    r2 = Rectangle(5, 10)
    eq = RectangleEq()

    assert eq.equals(s1, r1)
    assert not eq.equals(s1, r2)


def test_rect_contains():
    rs = [Rectangle(5, 5), Rectangle(5, 10), Rectangle(10, 10)]
    eq = RectangleEq()

    assert contains(eq)(rs, Rectangle(5, 10))
    assert not contains(eq)(rs, Rectangle(10, 5))


def test_square_contains():
    ss = [Square(5), Square(10)]
    eq = RectangleEq()

    assert contains(eq)(ss, Square(5))
    assert not contains(eq)(ss, Square(15))
