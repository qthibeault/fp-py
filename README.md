# FP-Py

Functional programming concepts implemented in Python.
This library takes inspiration from [fp-ts](https://github.com/gcanti/fp-ts).

## Installation

Install this library using `pip` by running the command `pip install git+https://gitlab.com/qthibeault/fp-py.git`.

## Usage

This library is broken up into modules that define common type-classes.
Each type class is represented as a Python protocol, and can be implemented by providing the specified methods.

## Example

```python
from dataclasses import dataclass
from fp_py.eq import Eq, contains

@dataclass
class Rectangle:
    length: float
    width: float

class RectangleEq(Eq[Rectangle]):
    def equals(self, a: Rectangle, b: Rectangle) -> bool:
        return a.length == b.length and a.width == b.width

r1 = Rectangle(10, 15)
r2 = Rectangle(1, 4)
r3 = Rectangle(5, 5)
rs = [r1, r2]
eq = RectangleEq()

assert contains(eq)(rs, r1)
assert not contains(eq)(rs, r3)
```

